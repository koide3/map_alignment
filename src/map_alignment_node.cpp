#include <mutex>
#include <regex>
#include <memory>
#include <fstream>
#include <iostream>

#include <mrpt/base.h>
#include <mrpt/slam/CICP.h>
#include <mrpt/maps/CSimplePointsMap.h>

#include <pcl/point_types.h>
#include <pcl/point_cloud.h>
#include <pcl_ros/transforms.h>
#include <pcl_ros/point_cloud.h>

#include <ros/ros.h>
#include <ros/package.h>
#include <tf/transform_listener.h>
#include <tf/transform_broadcaster.h>
#include <nav_msgs/OccupancyGrid.h>
#include <sensor_msgs/PointCloud2.h>
#include <geometry_msgs/PoseWithCovarianceStamped.h>
#include <tf_conversions/tf_eigen.h>
#include <eigen_conversions/eigen_msg.h>
#include <pcl_conversions/pcl_conversions.h>


class MapAlignmentNode {
public:
  MapAlignmentNode()
    : nh(),
      private_nh("~")
  {
    points_pub = private_nh.advertise<sensor_msgs::PointCloud2>("camera_points", 1, true);
    map_points_pub = private_nh.advertise<sensor_msgs::PointCloud2>("map_points", 1, true);

    points_topics_pattern = private_nh.param<std::string>("points_topics_pattern", ".*/depth_ir/points");
    world_frame_id = private_nh.param<std::string>("world_frame_id", "/world");
    robot_map_topic = private_nh.param<std::string>("map_frame_id", "/map");

    launch_file_name = private_nh.param<std::string>("output_launch_filename", "/launch/publish_robot2camera_tf.launch");

    min_height = private_nh.param<double>("min_height", 0.2);
    max_height = private_nh.param<double>("max_height", 0.4);

    retrieve_robot_map();
    generate_camera_map();

    robot2camera.setIdentity();
    tf_timer = nh.createTimer(ros::Duration(0.5), boost::bind(&MapAlignmentNode::tf_timer_callback, this, _1));

    initialpose_sub = nh.subscribe("/initialpose", 5, &MapAlignmentNode::initialpose_callback, this);

    std::cout << "done" << std::endl;
  }

private:

  /**
   * @brief find topics which meets #points_topics_pattern
   * @return topic names
   */
  std::vector<std::string> find_points_topics() const {
    std::regex pattern(points_topics_pattern);
    std::cout << "points_topics_pattern: " << points_topics_pattern << std::endl;

    ros::master::V_TopicInfo topic_infos;
    ros::master::getTopics(topic_infos);

    std::vector<std::string> topics;
    for(const auto& topic_info: topic_infos) {
      if(std::regex_match(topic_info.name, pattern)){
        topics.push_back(topic_info.name);
      }
    }

    return topics;
  }

  /**
   * @brief retrieve points from topics (wait until one PointCloud2 is received for each topic)
   * @param topics  topic names
   * @return obtained point cloud in the world frame
   */
  pcl::PointCloud<pcl::PointXYZ>::Ptr retrieve_points(const std::vector<std::string>& topics) {
    ros::Duration(1.0).sleep();

    pcl::PointCloud<pcl::PointXYZ>::Ptr accumulated(new pcl::PointCloud<pcl::PointXYZ>());
    std::cout << "retrieve points from:" << std::endl;
    // for each topic
    for(const auto& topic : topics) {
      std::cout << topic << std::endl;

      std::cout << "- waiting for message..." << std::endl;
      auto cloud_msg = ros::topic::waitForMessage<sensor_msgs::PointCloud2>(topic, nh, ros::Duration(15.0));

      std::cout << "- waiting for transform..." << std::endl;
      listener.waitForTransform(world_frame_id, cloud_msg->header.frame_id, cloud_msg->header.stamp, ros::Duration(5.0));

      std::cout << "- transforming cloud..." << std::endl;
      sensor_msgs::PointCloud2 transformed_msg;
      pcl_ros::transformPointCloud(world_frame_id, *cloud_msg, transformed_msg, listener);

      pcl::PointCloud<pcl::PointXYZ>::Ptr cloud(new pcl::PointCloud<pcl::PointXYZ>());
      pcl::fromROSMsg(transformed_msg, *cloud);
      std::copy(cloud->begin(), cloud->end(), std::back_inserter(accumulated->points));
    }

    std::cout << "total " << accumulated->size() << " points received!!" << std::endl;
    accumulated->header.frame_id = world_frame_id;
    accumulated->height = accumulated->size();
    accumulated->width = 1;
    accumulated->is_dense = false;

    return accumulated;
  }

  /**
   * @brief extract points in a certain height range
   * @param points  input point cloud
   * @return extracted point cloud
   */
  std::shared_ptr<mrpt::maps::CSimplePointsMap> extract_points(pcl::PointCloud<pcl::PointXYZ>::Ptr& points) const {
    std::cout << "extracing points" << std::endl;

    auto remove_loc = std::remove_if(points->begin(), points->end(), [=](const pcl::PointXYZ& pt) {
      return std::isnan(pt.x) || std::isnan(pt.y) || std::isnan(pt.z) || pt.z < min_height || pt.z > max_height;
    });
    points->erase(remove_loc, points->end());

    points->height = points->size();
    points->width = 1;

    std::shared_ptr<mrpt::maps::CSimplePointsMap> pointsmap(new mrpt::maps::CSimplePointsMap());
    for(auto& point : points->points) {
      pointsmap->insertPoint(point.x, point.y);
    }

    return pointsmap;
  }

  /**
   * @brief generate a 2D map from point clouds obtained from cameras
   */
  void generate_camera_map() {
    std::cout << "*** generate camera map ***" << std::endl;
    std::vector<std::string> topics = find_points_topics();
    pcl::PointCloud<pcl::PointXYZ>::Ptr accumulated = retrieve_points(topics);
    camera_map = extract_points(accumulated);

    points_pub.publish(accumulated);
  }

  /**
   * @brief retrieve a (robot's) 2D map
   */
  void retrieve_robot_map() {
    std::cout << "*** retrieve robot map ***" << std::endl;
    auto map_msg = ros::topic::waitForMessage<nav_msgs::OccupancyGrid>(robot_map_topic, nh, ros::Duration(15.0));
    robot_map_frame_id = map_msg->header.frame_id;

    // here, we ignore orientation of the origin
    // because wiki of map_server tells me that it is ignored in most of functions...
    Eigen::Vector2d origin(map_msg->info.origin.position.x, map_msg->info.origin.position.y);
    std::cout << "map_origin " << origin.transpose() << std::endl;

    double cell2m = map_msg->info.resolution;
    int width = map_msg->info.width;
    int height = map_msg->info.height;

    // transform the robot map into the world frame
    robot_map.reset(new mrpt::maps::CSimplePointsMap());
    pcl::PointCloud<pcl::PointXYZ>::Ptr map_points(new pcl::PointCloud<pcl::PointXYZ>());
    for(int y=0; y<height; y++) {
      for(int x=0; x<width; x++) {
        // skip unknown (255) and free (0) pixels
        // and extract only occupied cells (100)
        unsigned char cell = map_msg->data[y * width + x];
        if(cell == 255 || cell == 0) {
          continue;
        }

        Eigen::Vector4d pt = Eigen::Vector4d(x, y, 0, 1) * cell2m;
        pt.head<2>() += origin;

        pcl::PointXYZ p;
        p.getVector3fMap() = pt.head<3>().cast<float>();
        map_points->push_back(p);

        robot_map->insertPoint(pt.x(), pt.y());
      }
    }

    map_points->header.frame_id = robot_map_frame_id;
    map_points->width = map_points->size();
    map_points->height = 1;
    map_points->is_dense = false;
    map_points_pub.publish(map_points);
  }

  /**
   * @brief publish transform between the world and the map frames
   * @param e
   */
  void tf_timer_callback(const ros::TimerEvent& e) {
    std::unique_lock<std::mutex> lock(mutex);
    tf::Transform transform;
    tf::transformEigenToTF(robot2camera, transform);
    broadcaster.sendTransform(tf::StampedTransform(transform, ros::Time::now(), world_frame_id, robot_map_frame_id));
  }

  /**
   * @brief callback for initial guess input from rviz
   * @param pose_msg
   */
  void initialpose_callback(const geometry_msgs::PoseWithCovarianceStamped::Ptr& pose_msg) {
    std::cout << "pose received" << std::endl;

    Eigen::Isometry3d initialpose;
    tf::poseMsgToEigen(pose_msg->pose.pose, initialpose);

    std::cout << "--- initialpose ---" << std::endl;
    std::cout << initialpose.matrix() << std::endl;

    mutex.lock();
    robot2camera = initialpose;
    mutex.unlock();

    Eigen::Vector3d initpos = initialpose.translation();
    double yaw = std::atan2(initialpose.linear()(0, 0), initialpose.linear()(1, 0));

    std::cout << "initguess (" << initpos[0] << ", " << initpos[1] << ", " << yaw << ")" << std::endl;

    std::cout << "applying ICP..." << std::endl;
    mrpt::slam::CICP icp;
    icp.options.ICP_algorithm = mrpt::slam::icpClassic;
    icp.options.maxIterations = 512;
    icp.options.thresholdAng = 0.1;
    icp.options.thresholdDist = 0.75;
    icp.options.smallestThresholdDist = 0.01;
    icp.options.doRANSAC = false;
    icp.options.dumpToConsole();

    mrpt::poses::CPose2D initial_guess(initpos.x(), initpos.y(), yaw);
    mrpt::poses::CPosePDFPtr estimated;
    try{
      estimated = icp.Align(camera_map.get(), robot_map.get(), initial_guess);
    } catch (std::exception& e) {
      std::cerr << "failed to align!!" << std::endl;
      std::cerr << e.what() << std::endl;
      return;
    }

    mrpt::poses::CPose2D mean = estimated->getMeanVal();
    std::cout << mean << std::endl;

    std::unique_lock<std::mutex> lock(mutex);
    std::cout << mean.x() << ", " << mean.y() << ", " << mean.phi() << std::endl;
    robot2camera.translation() = Eigen::Vector3d(mean.x(), mean.y(), 0.0);
    robot2camera.linear() = Eigen::AngleAxisd(mean.phi(), Eigen::Vector3d::UnitZ()).toRotationMatrix();

    save();
  }

  /**
   * @brief save the relational pose between the world and map frames to the launch file
   */
  void save() {
    std::string package_path = ros::package::getPath("map_alignment");

    Eigen::Vector3d pos = robot2camera.translation();
    Eigen::Quaterniond quat(robot2camera.linear());

    std::ofstream ofs(package_path + launch_file_name);
    ofs << "<?xml version=\"1.0\"?>\n"
        << "<launch>\n"
        << "<node pkg=\"tf\" name=\"robot2camera_publisher\" type=\"static_transform_publisher\" args=\""
        << pos.x() << " " << pos.y() << " " << pos.z() << " " << quat.x() << " " << quat.y() << " " << quat.z() << " " << quat.w() << " "
        << world_frame_id << " " << robot_map_frame_id << " 10\"/>\n"
        << "</launch>" << std::endl;
  }

private:
  // ROS related
  ros::NodeHandle nh;
  ros::NodeHandle private_nh;
  tf::TransformListener listener;
  tf::TransformBroadcaster broadcaster;

  ros::Timer tf_timer;
  ros::Publisher points_pub;
  ros::Publisher map_points_pub;

  ros::Subscriber initialpose_sub;

  std::string points_topics_pattern;
  std::string world_frame_id;

  std::string robot_map_topic;
  std::string robot_map_frame_id;

  std::string launch_file_name;

  double min_height;
  double max_height;

  std::mutex mutex;
  Eigen::Isometry3d robot2camera;

  std::shared_ptr<mrpt::maps::CSimplePointsMap> camera_map;
  std::shared_ptr<mrpt::maps::CSimplePointsMap> robot_map;
};


int main(int argc, char** argv) {
  ROS_INFO("map_alignment_node");
  ros::init(argc, argv, "map_alignment_node");

  std::unique_ptr<MapAlignmentNode> node(new MapAlignmentNode());
  ros::spin();
  return 0;
}
