# map_alignment

## Overview

This is a package to align a 2D map with an OpenPTrack's camera network coordinate. It receives an initial guess from rviz's "2D pose estimate" button, and then applies ICP between the map and the point clouds obtained from the camera network.

The map_alignment package has been tested under [ROS] Kinetic and Ubuntu 16.04.

![screenshot](data/screenshot0.jpg "screenshot")

## Installation

```bash
sudo apt-get install mrpt-common
```

## Usage

Start all the openptrack detectors and tracker.

```bash
roslaunch detection detection_node_...
```

```bash
roslaunch tracking tracking_node.launch
```

Start *map_server* to provide the 2D grid map to be aligned to the camera network.

```bash
rosrun map_server map_server map.yaml
```

Start *rviz* to visualize the 2D map and point clouds obtained from the camera network.

```bash
roscd map_alignment/rviz
rviz -d alignment.rviz
```

Run *map_alignment_node*. It automatically detects all the RGB-D sensors in the camera network and retrieves point clouds from them. It also retrieves a 2D map from *map_server*.

```bash
rosrun map_alignment map_alignment_node
```

After data retrieving finished, you can give an initial guess using "2D pose estimate" on rviz. The estimated transform will be written to *map_alignment/lanuch/publish_robot2camera_tf.launch*. If the 2D map is correctly aligned to the camera network, terminate *map_alignment_node*.


You can launch this to publish tf transform between the robot's map and the camera network.

```bash
roslaunch map_alignment publish_robot2camera_tf.launch
```


## Nodes

### map_alignment_node

Aligns a 2D grid map with a camera network.


#### Subscribed Topics

* **`/map`** ([nav_msgs/OccupancyGrid])

	(Robot's) 2D map to be aligned with the camera network.


#### Parameters

* **`points_topics_pattern`** (string, default: ".*/depth_ir/points")

	A regex pattern to find points topics.

* **`output_launch_filename`** (string, default: "/launch/publish_robot2camera_tf.launch")

	Where the estimated transformation will be written.



